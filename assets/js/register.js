console.log("hello from JS file"); 

let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener('submit', (event) => {
    event.preventDefault() //to avoid page refresh/redirection once that the event has been triggered.

    //capture each values inside the input fields. 
	let firstName = document.querySelector("#firstName").value
	//console.log(firstName)
	let lastName = document.querySelector("#lastName").value
	//console.log(lastName)
	let userEmail = document.querySelector("#userEmail").value
	//console.log(userEmail)
	let mobileNo = document.querySelector("#mobileNumber").value
	//console.log(mobileNo)
	let password = document.querySelector("#password1").value
	console.log(password)
	let verifyPassword = document.querySelector("#password2").value
	console.log(verifyPassword)

	//information validation upon creating a new entry in the database.
	//lets create a control structure
	//=> to check if passwords match
	//=> to check if password are not empty
	//=> to check the validation for mobile Number, what we can do is to check the length of the mobile number input. 
	if((password !== "" && verifyPassword !== "") && (verifyPassword === password) && (mobileNo.length === 11)){
        fetch('https://encodeableph.herokuapp.com/api/users/email-exists', {
           method: 'POST',
           headers: {
           	  'Content-Type': 'application/json'
           },
           body: JSON.stringify({
           	   email: userEmail
           })
        }).then(res => res.json()
            ) //this will give the information if there are no duplicates found.
           .then(data => {
              if(data === false){
                 fetch('https://encodeableph.herokuapp.com/api/users/register', {
		            method: 'POST',
		            headers: {
		           	  'Content-Type': 'application/json'
		           },	
		           body: JSON.stringify({
		           	  firstName: firstName,
		           	  lastName: lastName,
		           	  email: userEmail, 
		           	  mobileNo: mobileNo,
		              password: password
		           }) //this section describe the body of the request converted into a JSON format. 
		        }).then(res => {
		        	return res.json()
		        }).then(data => {
		        	console.log(data)
		        	if(data === true){
		        		Swal.fire({
							icon: 'success',
							text: `Hey ${firstName}, glad to have you onboard!`,
							showConfirmButton: false,
							timer: 150000
						})
						window.location.replace('/frontend/pages/login.html') 
					}else {	
						Swal.fire({
							icon: 'error',
							title: 'Uh-oh!',
							text: 'Something went wrong. Please check.'
						})
		        	}
		        })
              }else{
				Swal.fire({
					icon: 'warning',
					title: 'Uh-oh!',
					text: 'The email is already in file. Please use another one.'
				})
              }
           })
	}else {
		Swal.fire({
			icon: 'error',
			title: 'Oooopss!!',
			text: 'Please check your input'
		})
	}
})