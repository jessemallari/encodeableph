console.log("hello from JS"); 

//THE first thing that we need to do is to identify which course it needs to display inside the browser. 
//we are going to use the course id to identify the correct course properly. 
let params = new URLSearchParams(window.location.search)
//window.location -> returns a location object with information about the "current" location of the document. 
// .search => contains the query string section of the current URL. //search property returns an object of type stringString.
//  URLSearchParams() -> this method/constructor creates and returns a URLSearchParams object.  (this is a class)
//"URLSearchParams" -> this describes the interface that defines utility methods to work with the query string of a URL.  (this is the prop type). 
// new -> instantiate a user-defined object. 
//yes it is a class -> template for creating the object.

//params = {
//  "courseId": "....id ng course that we passed"   
//}
let id = params.get('userId')
console.log(id)
let token = localStorage.getItem('token')
console.log(token)

//lets capture the sections of the html body
let name = document.querySelector("#courseName")
let desc = document.querySelector("#courseDesc")
let price = document.querySelector("#coursePrice")
let enroll = document.querySelector("#enrollmentContainer")

fetch(`https://encodeableph.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(data => {
    console.log(data)

    name.innerHTML = data.name
    desc.innerHTML = data.description
    price.innerHTML = data.price
	enroll.innerHTML = `<a href="#" id="enrollButton" class="btn btn-dark text-white btn-block"> Enroll details</a>`  

    document.querySelector("#enrollButton").addEventListener("click", () => {
        //insert the couse to our enrollment arrray inside the user collection 
        fetch('https://encodeableph.herokuapp.com/api/users/enroll', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}` // we have to get the value of auth string to verify and validate the user 
            },
            body : JSON.stringify({
                courseId: id
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            // now we can inform the user that the request has been done or not 
            if (data === true) {
                alert("Thank you for enrolling to this course");
                //redirect back to courses page
              window.location.replace('./courses.html') //still editing path 
            } else {
                //inform the user that the request has failed
                alert("something went wrong")
            }
        })
    })
}) 

//if the result is displayed in the console. it means you were able to properly pass the courseid and successfully created your fetch request. 

// enroll.addEventListener("click", (e)=> {
//     e.preventDefault();

//     if(!token || token === null){
//     //lets redirect the user to the login page
//     //alert("You must login first")
//         Swal.fire({
//             icon: 'error',
//             title: 'Oops...',
//             text: 'You must login first!',
//         }).then((result) => {
//             if (result.isConfirmed) window.location.href="./login.html"
//         })
//     }else {
//             Swal.fire({
//             title: 'Are you sure you want to enroll this course?',
//             icon: 'info',
//             showCancelButton: true,
//             confirmButtonColor: '#3085d6',
//             cancelButtonColor: '#d33',
//             confirmButtonText: 'Yes!'
//         }).then((result) => {
//             if (result.isConfirmed) {
//             Swal.fire(
//                 'Success!',
//                 'Course enrolled successfully',
//                 'success'
//             )
//         fetch('http://localhost:4000/api/users/enroll', {
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json',
//                 'Authorization': `Bearer ${token}`
//             },
//                 body: JSON.stringify({
//                 courseId: id
//             })
//             }).then(res => res.json())

//             }
//         })
//     }
// })