console.log("hello from script.js")
//capture the navSession element inside the navbar component
let navItem = document.querySelector('#navSession') 
let navItem2 = document.querySelector('#navSession2') 

//lets take the access token from the local storage property.
let userToken = localStorage.getItem("token")
// console.log(userToken)

//lets create a control structure that will determine which elements inside the nav bar will be displayes if a user token is found in the local storage.
if(!userToken) {
   navItem2.innerHTML =
   `
    <li class="nav-item mx-2">
    	<a href="./login.html" class="nav-link js-scroll-trigger">Login</a>
    </li>
   `
}else{
   navItem.innerHTML =
   `<a href="./profile.html" class="nav-link js-scroll-trigger">Profile</a>
   `
   navItem2.innerHTML =
   `
   <a href="./logout.html" class="nav-link js-scroll-trigger">LogOut</a>

   `

}